CREATE PROCEDURE `CarianByEmpNo`(IN empNo int)
BEGIN
select * from employees a inner join dept_emp b on b.emp_no=a.emp_no
inner join departments c on c.dept_no=b.dept_no
where a.emp_no=empNo;
END

-- Without Proper Message

CREATE PROCEDURE `CarianEmp`(IN empNo char(15),IN nama char(20), out message varchar(50))
BEGIN

if trim(nama) = '' && trim(empNo) = '' then
	set message = 'Carian terlarang';
else
	if trim(nama) <> '' then
	    select * from employees a inner join dept_emp b on b.emp_no=a.emp_no
		inner join departments c on c.dept_no=b.dept_no
		where 1=1 AND a.first_name LIKE nama;
	end if;
    
    if trim(empNo) <> '' then
	    select * from employees a inner join dept_emp b on b.emp_no=a.emp_no
		inner join departments c on c.dept_no=b.dept_no
		where 1=1 AND a.emp_no =empNo;
	end if;
    
    if trim(nama) <> '' && trim(empNo) <> '' then
	    select * from employees a inner join dept_emp b on b.emp_no=a.emp_no
		inner join departments c on c.dept_no=b.dept_no
		where 1=1 AND a.emp_no =empNo AND a.first_name LIKE nama;
	end if;
        
    set message = 'OK';
end if;



END

-- With Proper Message

CREATE  PROCEDURE `CarianEmp`(IN empNo char(15),IN nama char(20), out message varchar(50))
BEGIN
set @myc =0;
if trim(nama) = '' && trim(empNo) = '' then
	set message = 'Carian terlarang';
else
	if trim(nama) <> '' then
		select count(a.emp_no) into @myc from employees a inner join dept_emp b on b.emp_no=a.emp_no
		inner join departments c on c.dept_no=b.dept_no
		where 1=1 AND a.first_name LIKE nama;
        
        select * from employees a inner join dept_emp b on b.emp_no=a.emp_no
		inner join departments c on c.dept_no=b.dept_no
		where 1=1 AND a.first_name LIKE nama;
	end if;
    
    if trim(empNo) <> '' then
		select count(a.emp_no) into @myc  from employees a inner join dept_emp b on b.emp_no=a.emp_no
		inner join departments c on c.dept_no=b.dept_no
		where 1=1 AND a.emp_no =empNo;
        
        select * from employees a inner join dept_emp b on b.emp_no=a.emp_no
		inner join departments c on c.dept_no=b.dept_no
		where 1=1 AND a.emp_no =empNo;
	end if;
    
    if trim(nama) <> '' && trim(empNo) <> '' then
		select count(a.emp_no) into @myc  from employees a inner join dept_emp b on b.emp_no=a.emp_no
		inner join departments c on c.dept_no=b.dept_no
		where 1=1 AND a.emp_no =empNo AND a.first_name LIKE nama;
        
        select * from employees a inner join dept_emp b on b.emp_no=a.emp_no
		inner join departments c on c.dept_no=b.dept_no
		where 1=1 AND a.emp_no =empNo AND a.first_name LIKE nama;
	end if;
    	    
    set message = @myc;
end if;



END

-- SP with FUNCTION
CREATE  PROCEDURE `CarianEmp`(IN empNo char(15),IN nama char(20), out message varchar(50))
BEGIN
set @myc =0;
if trim(nama) = '' && trim(empNo) = '' then
	set message = 'Carian terlarang';
else
	if trim(nama) <> '' then
		select count(a.emp_no) into @myc from employees a inner join dept_emp b on b.emp_no=a.emp_no
		inner join departments c on c.dept_no=b.dept_no
		where 1=1 AND a.first_name LIKE nama;
        
        select a.emp_no,a.first_name,a.last_name, get_sal_level(a.emp_no) as `level` 
        from employees a inner join dept_emp b on b.emp_no=a.emp_no
		inner join departments c on c.dept_no=b.dept_no
		where 1=1 AND a.first_name LIKE nama;
	end if;
    
    if trim(empNo) <> '' then
		select count(a.emp_no) into @myc  from employees a inner join dept_emp b on b.emp_no=a.emp_no
		inner join departments c on c.dept_no=b.dept_no
		where 1=1 AND a.emp_no =empNo;
        
        select  a.emp_no,a.first_name,a.last_name, get_sal_level(a.emp_no) as `level`  
        from employees a inner join dept_emp b on b.emp_no=a.emp_no
		inner join departments c on c.dept_no=b.dept_no
		where 1=1 AND a.emp_no =empNo;
	end if;
    
    if trim(nama) <> '' && trim(empNo) <> '' then
		select count(a.emp_no) into @myc  from employees a inner join dept_emp b on b.emp_no=a.emp_no
		inner join departments c on c.dept_no=b.dept_no
		where 1=1 AND a.emp_no =empNo AND a.first_name LIKE nama;
        
        select a.emp_no,a.first_name,a.last_name, get_sal_level(a.emp_no) as `level`
        from employees a inner join dept_emp b on b.emp_no=a.emp_no
		inner join departments c on c.dept_no=b.dept_no
		where 1=1 AND a.emp_no =empNo AND a.first_name LIKE nama;
	end if;
    	    
    set message = @myc;
end if;



END